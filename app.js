const express = require('express')
const app = express()
const config = require('./config')
const history = require('connect-history-api-fallback')
const compression = require('compression')
const proxyMiddleware = require('http-proxy-middleware')

app.use(history())
app.use(compression())
Object.keys(config.build.proxyTable).forEach(key => {
  app.use(proxyMiddleware(key, config.build.proxyTable[key]))
})
app.use(express.static(config.build.assetsRoot))
app.listen(config.build.port || 3000, () => {
  console.log('app listening on port 3000.')
})
