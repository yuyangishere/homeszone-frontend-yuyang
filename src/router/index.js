import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
// 导入刚才编写的组件
import AppIndex from '@/components/home/AppIndex'
import Login from '@/components/Login'
import LoginError from '@/components/home/LoginError'
import Home from '@/components/home/Home'
import supply from '@/components/personal/supply'
import supplySuccess from '@/components/personal/supplySuccess'
import userRegister from '@/components/personal/userRegister'
import registerSuccess from '@/components/personal/registerSuccess'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      // home页面并不需要被访问
      redirect: '/index',
      children: [
        {
          path: '/index',
          name: 'AppIndex',
          component: AppIndex
          /*          meta: {
            requireAuth: true
          } */
        },
        {
          path: '/supply',
          name: 'supply',
          component: supply
          /*          meta: {
            requireAuth: true
          } */
        },
        {
          path: '/supplySuccess',
          name: 'supplySuccess',
          component: supplySuccess
          /*          meta: {
            requireAuth: true
          } */
        },
        {
          path: '/userRegister/:companyid/:companyname/',
          name: 'userRegister',
          component: userRegister
          /*          meta: {
            requireAuth: true
          } */
        },
        {
          path: '/registerSuccess',
          name: 'registerSuccess',
          component: registerSuccess
          /*          meta: {
            requireAuth: true
          } */
        }
      ]
    },
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/loginError',
      name: 'LoginError',
      component: LoginError
    }
  ]
})
