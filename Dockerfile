FROM node:10.16-alpine
WORKDIR /usr/src/app
RUN npm config set unsafe-perm true
RUN npm install -g cnpm --registry=https://registry.npm.taobao.org
COPY package*.json ./
RUN cnpm install
COPY . .
RUN cnpm run build
RUN rm -rf build src static
EXPOSE 7020
ENTRYPOINT [ "cnpm", "run" ]
